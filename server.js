var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

/**
 * Set up a basic Express server.
 */
server.listen(3000);

function logData(message) {
    var d = new Date();
    var time = `[${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}] `;
    console.log(time, message);
}

logData("Socket Server has booted...");
/**
 * Declare some variables for later use.
 */
var onlineUsers = {};
var plays = {
    player1: {
        done: false,
        sid: ''
    },
    player2: {
        done: false,
        sid: '',
    }
};

function lockOpen(socketId, forse) {
    io.emit('unlock', {
        socketId: socketId,
        username: onlineUsers[socketId],
        timestamp: new Date,
        forse: !!forse,
        type: 'message',
        message: onlineUsers[socketId] + ' is play the game!'
    });
    logData(onlineUsers[socketId] + ' is play the game!');
}

function alertMsg(socketId, msg, otherSocketMsg) {
    io.emit('message', {
        socketId: socketId,
        username: onlineUsers[socketId],
        timestamp: new Date,
        type: 'alert',
        message: msg,
        othersocketmsg: otherSocketMsg || ''
    });
    logData(msg);
}

function getActiveUsers(socketId, msg) {
    let numPlayers = Object.keys(onlineUsers).length;
    io.emit('users', {
        socketId: socketId,
        usernames: onlineUsers,
        timestamp: new Date,
        type: 'alert',
        message: msg || (numPlayers + ' player(s) has joined the game!')
    });
    logData(msg || (numPlayers + ' player(s) has joined the game!'));
}

function setPlayerSocket(sid) {
    if (!plays.player1.sid)
        plays.player1.sid = sid;
    else if (!plays.player2.sid)
        plays.player2.sid = sid;
    if (!(plays.player1.sid && plays.player2.sid)) {
        plays.player1.done = false;
        plays.player2.done = false;
    }
}

/**
 * Listen for a new connection on the server.
 */
io.on('connection', function(socket) {
    logData('Socket connected with ID of ' + socket.id);

    io.emit('connected', {
        socketId: socket.id,
        timestamp: new Date,
        type: 'alert',
        message: 'Socket connected with ID of ' + socket.id
    });
    /**
     * This will be fired when a user enters a username. The server will
     * associate the username with the socket ID and store it in,
     * $onlineUsers then emit a message to the client which
     * will in turn output a "XYZ has joined" message to
     * all connected users.
     */
    socket.on('join', function(username) {
        if (username === 'undefined')
            return;
        let numPlayers = Object.keys(onlineUsers).length;

        if (numPlayers == 2) {
            alertMsg(socket.id, 'Sorry, number of player exceeds!', 'Someone try to join');
            logData('Sorry, number of player exceeds!');
            return;
        } else if (numPlayers == 1) {
            let sid = Object.keys(onlineUsers)[0];
            if (username == onlineUsers[sid]) {
                console.log('Same username, try again');
                return;
            }
            //console.log('test', sid, onlineUsers[sid]);
            lockOpen(sid);
        }
        setPlayerSocket(socket.id);
        numPlayers++;
        logData(username + ' has connected!');
        onlineUsers[socket.id] = username;

        io.emit('joined', {
            socketId: socket.id,
            username: username,
            timestamp: new Date,
            type: 'alert',
            message: username + ' has joined the game!'
        });
        logData(username + ' has joined the game!');
        getActiveUsers(socket.id);

    });


    socket.on('send', function(score) {
        var senderUsername = onlineUsers[socket.id];

        var messageObject = {
            socketId: socket.id,
            username: senderUsername,
            timestamp: new Date,
            type: 'message',
            score: score
        };

        //console.log(senderUsername + ': ' + message.message);

        io.emit('score', messageObject);
    });

    socket.on('endplayergame', function(username) {
        var socketId = Object.keys(onlineUsers).filter(function(key) {
            return onlineUsers[key] == username;
        })[0];
        var secondSocketId = Object.keys(onlineUsers).filter(function(key) {
            return onlineUsers[key] != username;
        })[0];

        if (plays.player1.sid == socketId && !plays.player1.done) {
            lockOpen(secondSocketId, true);
            plays.player1.done = true;
        } else if (plays.player2.sid == socketId && !plays.player2.done) {
            io.emit('endgame', {
                socketId: socket.id,
                timestamp: new Date,
                type: 'alert',
                message: 'End Game'
            });
            logData('Game Over');
            plays.player2.done = true;
        }
    });

    /**
     * Gets fired when a user leaves the page. It will remove their session
     * from $onlineUsers and broadcast a message to the client to alert
     * everyone in the chat that the user has disconnected.
     */
    socket.on('disconnect', function() {
        var username = onlineUsers[socket.id];
        if (!username) return;
        delete onlineUsers[socket.id];

        if (plays.player1.sid == socket.id)
            plays.player1.sid = '';
        else if (plays.player2.sid == socket.id)
            plays.player2.sid = '';

        /*io.emit('userleft', {
            socketId: socket.id,
            username: username,
            timestamp: new Date,
            type: 'alert',
            message: username + ' has disconnected from the game!'
        });*/
        alertMsg(socket.id, username + ' has disconnected from the game.');
        logData(username + ' has disconnected from the game.');
        getActiveUsers(socket.id, username + ' player has leave');
    });
});