// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueSocketIO from 'vue-socket.io';
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'
import VueSession from 'vue-session'
import App from './App'

Vue.config.productionTip = false

Vue.use(VueSocketIO, 'http://localhost:3000')
Vue.use(VueMaterial)
Vue.use(VueSession)

/* eslint-disable no-new */
new Vue({
    el: '#app',
    template: '<App/>',
    components: { App }
})